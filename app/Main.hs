{-# LANGUAGE OverloadedStrings #-}

import           Crypto.Random
import           Database.PostgreSQL.Simple              (Connection, connect)
import           Network.Wai.Handler.Warp
import           Servant.Server.Experimental.Auth.Cookie

import           ConnectionConfig                        (connectInfo)
import           Server

-- | An IO action that connects to the database using the connection info from the ConnectionConfig module and prints logs as it does so
dbConnection :: IO Connection
dbConnection = putStrLn "Connecting to the database..."
            >> connect connectInfo
            >>= (>>) (putStrLn "Connected to the database!") . return

-- | The port number for running the server
port :: Int
port = 3000

-- | Says that it's starting the server, connects to the database, makes a random source and a persistent server key, and runs the app.
main :: IO ()
main = putStrLn ("Starting the server on port " ++ show port)
    >> dbConnection >>= \conn -> mkRandomSource drgNew numOfBytes
                                 >>= run port . app conn (mkPersistentServerKey "<<please, make me not persistent>>")
    where numOfBytes = 2000
