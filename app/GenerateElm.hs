{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE StandaloneDeriving #-}

import           Data.Text
import           Elm
import           GHC.Generics
import           Servant.API.Header
import           Servant.API.ResponseHeaders
import           Servant.Elm
import           Servant.Server.Experimental.Auth.Cookie

import           Model.AdminPost
import           Model.Skill
import           Model.User
import           Model.UserSkill
import           Server

-- | Takes a proxy of a type convertable to an elm type and a list of strings.
-- Adds Elm type definition, decoder and encoder to json to the beginning of the list
-- and returns the list.
makeElmType :: ElmType a => Proxy a -> [Text] -> [Text]
makeElmType proxy l = toElmTypeSource    proxy
                    : toElmDecoderSource proxy
                    : toElmEncoderSource proxy
                    : l

-- | Generates an Elm module (Generated.Api) for calling the API.
elmCode :: Spec
elmCode = Spec ["Generated", "Api"]
        $ defElmImports
        : "import Date exposing (..)"
        : "import Common.Date exposing (..)"
        : toElmTypeSource    (Proxy :: Proxy NewUser) -- the generated encoder doesn't work
        : toElmDecoderSource (Proxy :: Proxy NewUser) -- so I didn't generate it and wrote my own
        : makeElmType (Proxy :: Proxy AdminPostRead)
        ( makeElmType (Proxy :: Proxy NewAdminPost)
        $ makeElmType (Proxy :: Proxy SkillWrite)
        $ makeElmType (Proxy :: Proxy PublicUser)
        $ makeElmType (Proxy :: Proxy SkillWithPercentageRead)
        $ makeElmType (Proxy :: Proxy NewUserSkill)
        $ generateElmForAPI apiForClientSide)

-- | Outputs the generated Elm code to client/src/Generated/Api.elm
main :: IO ()
main = specsToDir [elmCode] "client/src"
