module Page.SignUp exposing (..)

{-| This module is for the sign up page.


# Model

@docs Model


# Initialization

@docs init


# Update

@docs Msg, update


# Commands

@docs encodeNewUser, signUpCmd


# Rendering

@docs view

-}

import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Bootstrap.Grid as Grid
import Date exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Generated.Api exposing (..)
import Common.PageLoadError exposing (..)
import Common.Date exposing (dateToString)
import Json.Encode as Encode
import Http
import Task
import Date


{-| Represents a new user and a list of locations (to select location when signin up).
-}
type alias Model =
    { newUser : NewUser
    , birthdayTmp : String
    }


{-| Attempts to get the current time. If succeds
gives initial model, otherwise shows the errors.
-}
init : Task.Task PageLoadError Model
init =
    Date.now
        |> Task.map
            (\now ->
                { newUser =
                    { newUserEmail = ""
                    , newUserPassword = ""
                    , newUserAddress = ""
                    , newUserAddress2 = Nothing
                    , newUserAddress3 = Nothing
                    , newUserBirthday = now
                    , newUserName = ""
                    , newUserPhone = ""
                    , newUserPostcode = ""
                    }
                , birthdayTmp = ""
                }
            )
        |> Task.mapError
            (\e ->
                Debug.log (toString e) <|
                    pageLoadError ("Something went wrong on the sign up page..." ++ toString e)
            )


{-| Represents one of the following events:
Change* for change of input.
SignUp for submiting the sign up form.
SignedUp for receiving the response from the server.
-}
type Msg
    = ChangeEmail String
    | ChangePassword String
    | ChangeName String
    | ChangePhone String
    | ChangeAddress String
    | ChangeAddress2 String
    | ChangeAddress3 String
    | ChangePostcode String
    | ChangeBirthday String
    | SignUp
    | SignedUp (Result Http.Error ())


{-| Takes a message and a model and returns model and
a command. If the given message is Change* then
it updates the corresponding field in the model.
-}
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        user =
            model.newUser
    in
        case msg of
            ChangeEmail str ->
                { model | newUser = { user | newUserEmail = str } } ! []

            ChangePassword str ->
                { model | newUser = { user | newUserPassword = str } } ! []

            ChangeName str ->
                { model | newUser = { user | newUserName = str } } ! []

            ChangePhone str ->
                { model | newUser = { user | newUserPhone = str } } ! []

            ChangeAddress str ->
                { model | newUser = { user | newUserAddress = str } } ! []

            ChangeAddress2 str ->
                if str == "" then
                    { model | newUser = { user | newUserAddress2 = Nothing } } ! []
                else
                    { model | newUser = { user | newUserAddress2 = Just str } } ! []

            ChangeAddress3 str ->
                if str == "" then
                    { model | newUser = { user | newUserAddress3 = Nothing } } ! []
                else
                    { model | newUser = { user | newUserAddress3 = Just str } } ! []

            ChangePostcode str ->
                { model | newUser = { user | newUserPostcode = str } } ! []

            ChangeBirthday str ->
                case Date.fromString str of
                    Ok date ->
                        { model | newUser = { user | newUserBirthday = date }, birthdayTmp = str } ! []

                    Err str ->
                        { model | birthdayTmp = str } ! []

            SignUp ->
                model ! [ signUpCmd model.newUser ]

            SignedUp (Ok ()) ->
                model ! []

            SignedUp (Err e) ->
                Debug.log (toString e) model ! []



-- commands


{-| Takes sign up details and encodes it.
-}
encodeNewUser : NewUser -> Encode.Value
encodeNewUser user =
    Encode.object
        [ ( "newUserEmail", Encode.string user.newUserEmail )
        , ( "newUserPassword", Encode.string user.newUserPassword )
        , ( "newUserAddress", Encode.string user.newUserAddress )
        , ( "newUserAddress2", (Maybe.withDefault Encode.null << Maybe.map Encode.string) user.newUserAddress2 )
        , ( "newUserAddress3", (Maybe.withDefault Encode.null << Maybe.map Encode.string) user.newUserAddress3 )
        , ( "newUserBirthday", Encode.string <| dateToString user.newUserBirthday )
        , ( "newUserName", Encode.string user.newUserName )
        , ( "newUserPhone", Encode.string user.newUserPhone )
        , ( "newUserPostcode", Encode.string user.newUserPostcode )
        ]


{-| Takes sign up details and sends a post request to server (/api/sign_up) to sign up.
-}
signUpCmd : NewUser -> Cmd Msg
signUpCmd details =
    Http.send SignedUp <|
        Http.request
            { method = "POST"
            , headers = []
            , url = "/api/sign_up"
            , body = Http.jsonBody <| encodeNewUser details
            , expect = Http.expectStringResponse << always <| Ok ()
            , timeout = Nothing
            , withCredentials = True
            }


{-| Shows the form for signing up.
-}
view : Model -> Html Msg
view model =
    let
        user =
            model.newUser
    in
        Grid.container []
            [ Form.form
                [ onSubmit SignUp ]
                [ Form.label [] [ text "Name" ]
                , Input.text [ Input.value user.newUserName, Input.onInput ChangeName ]
                , Form.label [] [ text "Email" ]
                , Input.text [ Input.value user.newUserEmail, Input.onInput ChangeEmail ]
                , Form.label [] [ text "Password" ]
                , Input.password [ Input.value user.newUserPassword, Input.onInput ChangePassword ]
                , Form.label [] [ text "Phone Number" ]
                , Input.text [ Input.value user.newUserPhone, Input.onInput ChangePhone ]
                , Form.label [] [ text "Address (Line 1)" ]
                , Input.text [ Input.value user.newUserAddress, Input.onInput ChangeAddress ]
                , Form.label [] [ text "Address (Line 2) (optional)" ]
                , Input.text [ Input.value <| Maybe.withDefault "" user.newUserAddress2, Input.onInput ChangeAddress2 ]
                , Form.label [] [ text "Address (Line 3) (optional)" ]
                , Input.text [ Input.value <| Maybe.withDefault "" user.newUserAddress3, Input.onInput ChangeAddress3 ]
                , Form.label [] [ text "Postcode" ]
                , Input.text [ Input.value user.newUserPostcode, Input.onInput ChangePostcode ]
                , Form.label [] [ text "Birthday" ]
                , Input.date [ Input.value model.birthdayTmp, Input.onInput ChangeBirthday ]
                , input [ type_ "submit", value "Sign Up", class "btn btn-primary" ] []
                ]
            ]
