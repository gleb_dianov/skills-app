module Page.Admin exposing (..)

{-| The module for initializing, updating, and viewing the admin page.


# Model

@docs init, Model


# Update

@docs Msg, update


# Rendering

@docs view

-}

import Task
import Html exposing (..)
import Generated.Api exposing (..)


{-| Placeholder
-}
type Model
    = Model


{-| Placeholder
-}
init : PublicUser -> Task.Task x Model
init currentUser =
    Task.succeed Model


{-| Placeholder
-}
type Msg
    = Msg


{-| Placeholder
-}
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Msg ->
            model ! []


{-| Placeholder
-}
view : Model -> Html Msg
view model =
    div [] [ text "Welcome to the Admin Page!" ]
