module Page.Login exposing (..)

{-| The module for the login page.


# Model

@docs LoginDetails, encodeLoginDetails, Model


# Initialization

@docs init


# Update

@docs Msg, update


# Commands

@docs loginCmd


# Rendering

@docs view

-}

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Bootstrap.Grid as Grid
import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Json.Encode as Encode
import Http
import Task
import Common.PageLoadError exposing (..)


-- model


{-| Represents login details: email, password.
-}
type alias LoginDetails =
    { email : String
    , password : String
    }


{-| Takes login details and encodes it.
-}
encodeLoginDetails : LoginDetails -> Encode.Value
encodeLoginDetails login =
    Encode.object
        [ ( "loginEmail", Encode.string login.email )
        , ( "loginPassword", Encode.string login.password )
        ]


{-| Just wraps login details.
-}
type alias Model =
    { login : LoginDetails
    }



-- init


{-| Initial login details
-}
init : Task.Task PageLoadError Model
init =
    Task.succeed { login = { email = "", password = "" } }



-- update


{-| Represents one of the following actions:
changing email or password, logging in, or
getting response from the server after requesting
logging in.
-}
type Msg
    = ChangeEmail String
    | ChangePassword String
    | LogIn
    | LoggedIn (Result Http.Error ())


{-| Takes a message and a model and returns a model and a command.
If the given message is Change* then it updates the corresponding
field of login field of the model. If the message is login then
it doesn't change the model but sends a log in command. When the
message is LoginResponse it logs errors if there are any.
-}
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangeEmail str ->
            { model | login = { email = str, password = model.login.password } } ! []

        ChangePassword str ->
            { model | login = { email = model.login.email, password = str } } ! []

        LoggedIn (Ok ()) ->
            model ! []

        LoggedIn (Err err) ->
            Debug.log (toString err) model ! []

        LogIn ->
            model ! [ loginCmd model.login ]



-- commands


{-| Takes login details and sends a post request to server (/api/login) to log in.
-}
loginCmd : LoginDetails -> Cmd Msg
loginCmd login =
    Http.send LoggedIn <|
        Http.request
            { method = "POST"
            , headers = []
            , url = "/api/login"
            , body = Http.jsonBody <| encodeLoginDetails login
            , expect = Http.expectStringResponse << always <| Ok ()
            , timeout = Nothing
            , withCredentials = True
            }


{-| Shows a form for login details, a log in button, and a link to the sign up page.
-}
view : Model -> Html Msg
view model =
    Grid.container []
        [ Form.form
            [ onSubmit LogIn ]
            [ Form.row []
                [ Form.colLabel [] [ text "Email: " ]
                , Form.colLabel [] [ text "Password" ]
                ]
            , Form.row []
                [ Form.col [] [ Input.text [ Input.onInput ChangeEmail ] ]
                , Form.col [] [ Input.password [ Input.onInput ChangePassword ] ]
                ]
            , br [] []
            , input [ type_ "submit", value "Login", class "btn btn-primary" ] []
            ]
        ]
