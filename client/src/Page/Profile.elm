module Page.Profile exposing (..)

{-| Module for the profile page.


# Model

@docs Editor, EditorModel, Model


# Initialization

@docs init


# Update

@docs Msg, update


# Commands

@docs getSkills


# Rendering

@docs showEditor, showPublicUser, showSkill, view

-}

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Table as Table
import Bootstrap.Modal as Modal
import Bootstrap.Button as Button
import Date.Extra.Format exposing (..)
import Http
import Task
import Common.PageLoadError as PageLoadError exposing (PageLoadError, pageLoadError)
import Generated.Api exposing (..)
import Page.Profile.UserDetailsEditor as UserDetailsEditor
import Page.Profile.SkillsEditor as SkillsEditor


-- model


{-| Represents an editor.
-}
type Editor
    = None
    | UserDetails
    | Skills


{-| Represents a model for an editor.
-}
type EditorModel
    = NilModel
    | UserDetailsEditorModel UserDetailsEditor.Model
    | SkillsEditorModel SkillsEditor.Model
    | EditorError PageLoadError


{-| Represents the current user, current user's skills,
an editor model, and state of the modal for displaying the editors.
-}
type alias Model =
    { currentUser : PublicUser
    , skills : List SkillWithPercentage
    , editorModel : EditorModel
    , modalState : Modal.State
    }



-- init


{-| Takes a user, sends 2 requests to the server to get the name of
s/he's selected location by location id and the user's skills, then
it gives the initial model using this data.
-}
init : PublicUser -> Task.Task PageLoadError Model
init user =
    Http.toTask (getApiSkillsForUserByUserId user.publicUserId)
        |> Task.map
            (\skills ->
                { skills = skills
                , editorModel = NilModel
                , modalState = Modal.hiddenState
                , currentUser = user
                }
            )
        |> Task.mapError (\e -> pageLoadError <| toString e)



-- update


{-| Represents one of the following events:
getting skills from the server, opening or closing an editor,
loading an editor, a user details editor event, the modal's
for displaying editors event.
-}
type Msg
    = GotSkills (Result Http.Error (List SkillWithPercentage))
    | ChangeEditor Editor
    | LoadedEditor (Result PageLoadError EditorModel)
    | UserDetailsEditorMsg UserDetailsEditor.Msg
    | SkillsEditorMsg SkillsEditor.Msg
    | ModalMsg Modal.State


{-| Takes a message and a model. If the message is GotSkills
it logs the errors if there are any, otherwise it updates the
skills field with the received data. If the given message is
ModalMsg then the function updates the modal state. If the
message is ChangeEditor then it changes opens or closes
the right editor. If the message is LoadedEditor then
it updates the editor model. If it's UserDetailsEditorMsg
then it just passes the messsage to the update function
of the user details editor.
-}
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        toPageMsg toModel toMsg subUpdate subMsg subModel =
            let
                ( newModel, newCmd ) =
                    subUpdate subMsg subModel
            in
                ( { model | editorModel = toModel newModel }
                , Cmd.map toMsg newCmd
                )
    in
        case msg of
            GotSkills (Err e) ->
                Debug.log (toString e) model ! []

            GotSkills (Ok skills) ->
                { model | skills = skills } ! []

            ModalMsg state ->
                { model | modalState = state } ! []

            ChangeEditor None ->
                update (ModalMsg Modal.hiddenState) { model | editorModel = NilModel }

            ChangeEditor newEditor ->
                case newEditor of
                    UserDetails ->
                        model
                            ! [ UserDetailsEditor.init model.currentUser
                                    |> Task.attempt (\result -> LoadedEditor <| Result.map UserDetailsEditorModel result)
                              ]

                    Skills ->
                        model
                            ! [ SkillsEditor.init model.currentUser model.skills
                                    |> Task.attempt (\result -> LoadedEditor <| Result.map SkillsEditorModel result)
                              ]

                    None ->
                        update (ModalMsg Modal.hiddenState) { model | editorModel = NilModel }

            LoadedEditor (Ok editorModel) ->
                case editorModel of
                    UserDetailsEditorModel _ ->
                        update (ModalMsg Modal.visibleState) { model | editorModel = editorModel }

                    SkillsEditorModel _ ->
                        update (ModalMsg Modal.visibleState) { model | editorModel = editorModel }

                    _ ->
                        model ! []

            LoadedEditor (Err e) ->
                { model | editorModel = EditorError e } ! []

            UserDetailsEditorMsg subMsg ->
                case model.editorModel of
                    UserDetailsEditorModel subModel ->
                        toPageMsg UserDetailsEditorModel UserDetailsEditorMsg UserDetailsEditor.update subMsg subModel

                    _ ->
                        model ! []

            SkillsEditorMsg subMsg ->
                case model.editorModel of
                    SkillsEditorModel subModel ->
                        toPageMsg SkillsEditorModel SkillsEditorMsg SkillsEditor.update subMsg subModel

                    _ ->
                        model ! []



-- commands


{-| Takes a user and sends a request to ther server to get the user's skills.
-}
getSkills : PublicUser -> Cmd Msg
getSkills currentUser =
    Http.send GotSkills <| getApiSkillsForUserByUserId currentUser.publicUserId



-- view


{-| Takes a user and shows all s/he's info.
-}
showPublicUser : PublicUser -> Html Msg
showPublicUser user =
    Table.table
        { options = [ Table.bordered ]
        , thead = Table.simpleThead []
        , tbody =
            Table.tbody []
                [ Table.tr []
                    [ Table.td [] [ text "Name" ]
                    , Table.td [] [ text user.publicUserName ]
                    ]
                , Table.tr []
                    [ Table.td [] [ text "Email" ]
                    , Table.td [] [ text user.publicUserEmail ]
                    ]
                , Table.tr []
                    [ Table.td [] [ text "Phone" ]
                    , Table.td [] [ text user.publicUserPhone ]
                    ]
                , Table.tr []
                    [ Table.td [] [ text "Address" ]
                    , Table.td [] [ text user.publicUserAddress ]
                    ]
                , case user.publicUserAddress2 of
                    Nothing ->
                        Table.tr [] []

                    Just address ->
                        Table.tr []
                            [ Table.td [] [ text "Address (Line 2)" ]
                            , Table.td [] [ text address ]
                            ]
                , case user.publicUserAddress3 of
                    Nothing ->
                        Table.tr [] []

                    Just address ->
                        Table.tr []
                            [ Table.td [] [ text "Address (Line 3)" ]
                            , Table.td [] [ text address ]
                            ]
                , Table.tr []
                    [ Table.td [] [ text "Birthday" ]
                    , Table.td [] [ text <| utcIsoDateString user.publicUserBirthday ]
                    ]
                , Table.tr []
                    [ Table.td [] [ text "Post Code" ]
                    , Table.td [] [ text user.publicUserPostcode ]
                    ]
                , case user.publicUserIsAdmin of
                    True ->
                        Table.tr [] [ Table.td [] [ text "Admin" ], Table.td [] [ text "True" ] ]

                    False ->
                        Table.tr [] []
                ]
        }


{-| Takes a user's skill with percentage and shows the name
of the skill and the set percentage.
-}
showSkill : SkillWithPercentage -> Table.Row Msg
showSkill skill =
    Table.tr []
        [ Table.td [] [ text skill.usersSkill.skillName ]
        , Table.td [] [ text <| toString skill.skillPercentage ]
        ]


{-| Takes the model, shows a modal with the opened editor if an editor is opened.
-}
showEditor : Model -> Html Msg
showEditor model =
    case model.editorModel of
        UserDetailsEditorModel subModel ->
            Modal.config ModalMsg
                |> Modal.large
                |> Modal.h3 [] [ text "Details Editor" ]
                |> Modal.body []
                    [ Grid.containerFluid []
                        [ UserDetailsEditor.view subModel |> Html.map UserDetailsEditorMsg
                        ]
                    ]
                |> Modal.footer []
                    [ Button.button
                        [ Button.primary
                        , Button.attrs [ onClick <| UserDetailsEditorMsg UserDetailsEditor.SaveChanges ]
                        ]
                        [ text "Save Changes" ]
                    , Button.button
                        [ Button.outlinePrimary
                        , Button.attrs [ onClick <| ModalMsg Modal.hiddenState ]
                        ]
                        [ text "Close" ]
                    ]
                |> Modal.view model.modalState

        SkillsEditorModel subModel ->
            Modal.config ModalMsg
                |> Modal.large
                |> Modal.h3 [] [ text "Skills Editor" ]
                |> Modal.body []
                    [ Grid.containerFluid []
                        [ SkillsEditor.view subModel |> Html.map SkillsEditorMsg
                        ]
                    ]
                |> Modal.footer []
                    [ Button.button
                        [ Button.outlinePrimary
                        , Button.attrs [ onClick <| ModalMsg Modal.hiddenState ]
                        ]
                        [ text "Close" ]
                    ]
                |> Modal.view model.modalState

        EditorError errorPage ->
            PageLoadError.view errorPage

        NilModel ->
            text ""


{-| Shows current user's details and skills.
Also shows a button for opening the user details editor.
-}
view : Model -> Html Msg
view model =
    let
        currentUser =
            model.currentUser
    in
        div []
            [ showEditor model
            , Grid.container []
                [ Grid.row []
                    [ Grid.col [ Col.lg6, Col.md12 ]
                        [ h1 [ align "center" ] [ text "User" ]
                        , showPublicUser currentUser
                        , div [ align "center" ]
                            [ Button.button
                                [ Button.attrs [ onClick <| ChangeEditor UserDetails ]
                                , Button.primary
                                ]
                                [ text "Edit" ]
                            , br [] []
                            , a [ href "/api/users/deleteAccount" ] [ text "del" ]
                            ]
                        ]
                    , Grid.col [ Col.lg6, Col.md12 ]
                        [ h1 [ align "center" ] [ text "Skills" ]
                        , Table.table
                            { options = [ Table.bordered ]
                            , thead =
                                Table.thead []
                                    [ Table.tr []
                                        [ Table.th [] [ text "Name" ]
                                        , Table.th [] [ text "Percentage" ]
                                        ]
                                    ]
                            , tbody = Table.tbody [] <| List.map showSkill model.skills
                            }
                        , div [ align "center" ]
                            [ Button.button
                                [ Button.attrs [ onClick <| ChangeEditor Skills ]
                                , Button.primary
                                ]
                                [ text "Edit" ]
                            ]
                        ]
                    ]
                ]
            ]
