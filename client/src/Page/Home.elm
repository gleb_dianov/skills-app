module Page.Home exposing (..)

{-| The module for initializing, updating, and viewing the home page.


# Model

@docs Model


# Initializing

@docs init


# Updating

@docs Msg, update


# Commands

@docs postNewAdminPost


# Viewing

@docs showAdminPost, view

-}

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Bootstrap.Grid as Grid
import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Bootstrap.Card as Card
import Bootstrap.ListGroup as BList
import Http
import Task
import Generated.Api exposing (..)
import Common.PageLoadError exposing (..)


-- model


{-| Represents a list of existing posts and a new admin post.
-}
type alias Model =
    { adminPosts : List AdminPost
    , newAdminPost : NewAdminPost
    }



-- init


{-| A task that attempts to fetch the existing admin posts from the server and initializes a new admin post.
If it fails to fetch the posts then it shows the error page.
-}
init : Task.Task PageLoadError Model
init =
    Http.toTask getApiAdminPosts
        |> Task.map (\posts -> { adminPosts = posts, newAdminPost = { newAdminPostTitle = "", newAdminPostMessage = "" } })
        |> Task.mapError (\_ -> pageLoadError "Something went wrong on the Home page...")



-- update


{-| Represents one of the following actions:
admin posts fetch response, change of the new post's title,
change of the new post's message, posting a message,
result of posting a message.
-}
type Msg
    = LoadedAdminPosts (Result Http.Error (List AdminPost))
    | ChangeNewPostTitle String
    | ChangeNewPostMessage String
    | PostNewAdminPost
    | PostedNewAdminPost (Result Http.Error (Maybe AdminPost))


{-| Takes a message and a model then returns an updated model and commands.

If the message is LoadedAdminPosts it logs the error if it failed and sets
the adminPosts field of the model to the loaded list.

If the message is ChangeNewPostTitle or ChangeNewPostMessage
then it updates the newAdminPostMessage of the model.

If the message is PostNewAdminPost then it returns a command
that sends a request to the server that tries to add the new
admin post message.

If the message is PostedNewAdminPost message then it handles
the result of the post request. It logs errors if there are
any or adds the new post to the model.

-}
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LoadedAdminPosts (Err e) ->
            ( Debug.log (toString e) model, Cmd.none )

        LoadedAdminPosts (Ok posts) ->
            ( { model | adminPosts = posts }, Cmd.none )

        ChangeNewPostTitle string ->
            ( { model
                | newAdminPost =
                    { newAdminPostTitle = string, newAdminPostMessage = model.newAdminPost.newAdminPostMessage }
              }
            , Cmd.none
            )

        ChangeNewPostMessage string ->
            ( { model
                | newAdminPost =
                    { newAdminPostTitle = model.newAdminPost.newAdminPostTitle, newAdminPostMessage = string }
              }
            , Cmd.none
            )

        PostNewAdminPost ->
            ( model, postNewAdminPost model.newAdminPost )

        PostedNewAdminPost (Err e) ->
            ( Debug.log (toString e) model, Cmd.none )

        PostedNewAdminPost (Ok Nothing) ->
            ( Debug.log "An error occured. The post request didn't come through." model, Cmd.none )

        PostedNewAdminPost (Ok (Just post)) ->
            ( { newAdminPost = { newAdminPostTitle = "", newAdminPostMessage = "" }
              , adminPosts = model.adminPosts ++ [ post ]
              }
            , Cmd.none
            )



-- commands


{-| Takes a new admin post and returns a command that sends a post
request to the server to add the add admin post to the database.
-}
postNewAdminPost : NewAdminPost -> Cmd Msg
postNewAdminPost post =
    Http.send PostedNewAdminPost <| postApiAdminPost post



-- view


{-| Takes an admin post and converts it to the bootstrap list data type.
-}
showAdminPost : AdminPost -> BList.Item Msg
showAdminPost post =
    BList.li []
        [ Card.config [ Card.outlineInfo ]
            |> Card.headerH3 [] [ text post.adminPostTitle ]
            |> Card.block [] [ Card.text [] [ text post.adminPostMessage ] ]
            |> Card.view
        ]


{-| Shows a form for new admin posts and shows the existing admin posts.
-}
view : Model -> Html Msg
view model =
    div []
        [ Grid.container []
            [ Form.form
                [ onSubmit PostNewAdminPost ]
                [ Input.text [ Input.placeholder "Title", Input.onInput ChangeNewPostTitle ]
                , Input.text [ Input.placeholder "Message", Input.onInput ChangeNewPostMessage ]
                , input [ type_ "submit", value "Submit", class "btn btn-primary" ] []
                ]
            , br [] []
            , BList.ul (List.map showAdminPost model.adminPosts)
            ]
        ]
