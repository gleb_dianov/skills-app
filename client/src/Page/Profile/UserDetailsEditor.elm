module Page.Profile.UserDetailsEditor exposing (..)

{-| This modules is for the user details editor on the profile page.


# Model

@docs Model


# Initialization

@docs init


# Updating

@docs Msg, update


# Commands

@docs encodePublicUser, updateUserCmd


# Rendering

@docs view

-}

import Html exposing (..)
import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Generated.Api exposing (..)
import Common.PageLoadError exposing (..)
import Common.Date exposing (dateToString)
import Json.Encode as Encode
import Http
import Task


-- model


{-| Represents a user.
-}
type alias Model =
    { user : PublicUser
    }



-- init


{-| Takes the current user and returns a task that attempts to fetch all the locations.
-}
init : PublicUser -> Task.Task PageLoadError Model
init currentUser =
    Task.succeed
        { user = currentUser
        }



-- update


{-| Represents one of the following actions:
changing the name, changing the email address, changing the address,
changing the second line of the address, changing the third line of the address,
changing the phone number, changing the location, saving changes, getting result
from the server after saving changes.
-}
type Msg
    = ChangeName String
    | ChangeEmail String
    | ChangeAddress String
    | ChangeAddress2 String
    | ChangeAddress3 String
    | ChangePhone String
    | ChangePostcode String
    | SaveChanges
    | SavedChanges (Result Http.Error PublicUser)


{-| Takes a message and a model and returns the updated model and a command.
If the given message is Change* then it returns the model with the corresponding
user field updated. If the message is SaveChanges then it returns the model and
a command that updates the user info on the server side. When the SavedChanges
message is given it handles the response from the server after requesting
user info update.
-}
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        user =
            model.user
    in
        case msg of
            ChangeName str ->
                { model | user = { user | publicUserName = str } } ! []

            ChangeEmail str ->
                { model | user = { user | publicUserEmail = str } } ! []

            ChangeAddress str ->
                { model | user = { user | publicUserAddress = str } } ! []

            ChangeAddress2 str ->
                if str == "" then
                    { model | user = { user | publicUserAddress2 = Nothing } } ! []
                else
                    { model | user = { user | publicUserAddress2 = Just str } } ! []

            ChangeAddress3 str ->
                if str == "" then
                    { model | user = { user | publicUserAddress3 = Nothing } } ! []
                else
                    { model | user = { user | publicUserAddress3 = Just str } } ! []

            ChangePhone str ->
                { model | user = { user | publicUserPhone = str } } ! []

            ChangePostcode str ->
                { model | user = { user | publicUserPostcode = str } } ! []

            SaveChanges ->
                model ! [ updateUserCmd model.user ]

            SavedChanges (Err e) ->
                Debug.log (toString e) model ! []

            SavedChanges (Ok user) ->
                { model | user = user } ! []



-- commands


{-| Takes user details and encodes it.
-}
encodePublicUser : PublicUser -> Encode.Value
encodePublicUser user =
    Encode.object
        [ ( "publicUserId", Encode.int user.publicUserId )
        , ( "publicUserEmail", Encode.string user.publicUserEmail )
        , ( "publicUserAddress", Encode.string user.publicUserAddress )
        , ( "publicUserAddress2", (Maybe.withDefault Encode.null << Maybe.map Encode.string) user.publicUserAddress2 )
        , ( "publicUserAddress3", (Maybe.withDefault Encode.null << Maybe.map Encode.string) user.publicUserAddress3 )
        , ( "publicUserBirthday", Encode.string <| dateToString user.publicUserBirthday )
        , ( "publicUserName", Encode.string user.publicUserName )
        , ( "publicUserPhone", Encode.string user.publicUserPhone )
        , ( "publicUserPostcode", Encode.string user.publicUserPostcode )
        , ( "publicUserIsAdmin", Encode.bool user.publicUserIsAdmin )
        ]


{-| Takes user details and sends an update request to the server.
-}
updateUserCmd : PublicUser -> Cmd Msg
updateUserCmd details =
    Http.send SavedChanges <|
        Http.request
            { method = "PUT"
            , headers = []
            , url = "/api/users"
            , body = Http.jsonBody <| encodePublicUser details
            , expect = Http.expectStringResponse << always <| Ok details
            , timeout = Nothing
            , withCredentials = True
            }



-- view


{-| Shows a form with all the user fields.
-}
view : Model -> Html Msg
view model =
    let
        user =
            model.user
    in
        Form.form
            []
            [ Form.label [] [ text "Name" ]
            , Input.text [ Input.value user.publicUserName, Input.onInput ChangeName ]
            , Form.label [] [ text "Email" ]
            , Input.text [ Input.value user.publicUserEmail, Input.onInput ChangeEmail ]
            , Form.label [] [ text "Phone Number" ]
            , Input.text [ Input.value user.publicUserPhone, Input.onInput ChangePhone ]
            , Form.label [] [ text "Address (Line 1)" ]
            , Input.text [ Input.value user.publicUserAddress, Input.onInput ChangeAddress ]
            , Form.label [] [ text "Address (Line 2)" ]
            , Input.text [ Input.value <| Maybe.withDefault "" user.publicUserAddress2, Input.onInput ChangeAddress2 ]
            , Form.label [] [ text "Address (Line 3)" ]
            , Input.text [ Input.value <| Maybe.withDefault "" user.publicUserAddress3, Input.onInput ChangeAddress3 ]
            , Form.label [] [ text "Postcode" ]
            , Input.text [ Input.value user.publicUserPostcode, Input.onInput ChangePostcode ]
            ]
