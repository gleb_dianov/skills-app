module Page.Profile.SkillsEditor exposing (..)

{-| Module for the usersSkills editor for the profile page.


# Model

@docs SkillId, Percentage, Model


# Initialization

@docs init


# Update

@docs Msg, update, newSkillOption


# Commands

@docs associateSkill, deleteUserSkill, createUserSkill


# Rendering

@docs view

-}

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Bootstrap.Form as Form
import Bootstrap.Form.Select as Select
import Bootstrap.Form.Input as Input
import Bootstrap.Button as Button
import Bootstrap.Table as Table
import Generated.Api exposing (..)
import Common.PageLoadError exposing (..)
import Common.Date exposing (dateToString)
import Json.Encode as Encode
import Http
import Task


-- model


{-| Type synonym for Int.
-}
type alias SkillId =
    Int


{-| Type synonym for Int.
-}
type alias Percentage =
    Int


{-| Represents a user and their skills.
-}
type alias Model =
    { user : PublicUser
    , usersSkills : List SkillWithPercentage
    , skills : List Skill
    , newUserSkillId : Maybe SkillId
    , newSkillPercentage : Int
    , newSkill : Maybe Skill
    }



-- init


{-| Takes the current user and returns a task that tries to fetch all skills and initialise the model.
-}
init : PublicUser -> List SkillWithPercentage -> Task.Task PageLoadError Model
init currentUser usersSkills =
    Http.toTask getApiSkills
        |> Task.map
            (\skills ->
                { user = currentUser
                , usersSkills = usersSkills
                , skills = skills
                , newSkillPercentage = 1
                , newUserSkillId =
                    Maybe.andThen .skillId <|
                        List.head <|
                            List.filter
                                (\skill ->
                                    not <|
                                        List.member skill.skillId <|
                                            List.map (\swp -> swp.usersSkill.skillId) usersSkills
                                )
                                skills
                , newSkill = Nothing
                }
            )
        |> Task.mapError (\e -> pageLoadError <| toString e)



-- update


{-| Value for new skill for the selector.
-}
newSkillOption : String
newSkillOption =
    "new-skill"


{-| Represents one of the following events:
change of percentage or skill, submition of the form or receiving of a response from the server.
-}
type Msg
    = ChangePercentage String
    | ChangeSkill String
    | ChangeNewSkillName String
    | AssociateSkill
    | GotNewSkill (Result Http.Error (Maybe SkillWithPercentage))
    | DeleteSkill SkillWithPercentage
    | DeletedSkill SkillId (Result Http.Error Bool)


{-| If the given message is Change* it updates the corresponding field of the model.
If it is AssociateSkill then sends a request to the server with user id, skill id, and percentage.
If gets a response from the server with the new skill with percentage then adds it to the list.
-}
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangePercentage str ->
            case String.toInt str of
                Ok percentage ->
                    if percentage >= 100 then
                        { model | newSkillPercentage = 100 } ! []
                    else if percentage <= 1 then
                        { model | newSkillPercentage = 1 } ! []
                    else
                        { model | newSkillPercentage = percentage } ! []

                Err e ->
                    Debug.log e model ! []

        ChangeSkill str ->
            case String.toInt str of
                Ok id ->
                    { model | newUserSkillId = Just id } ! []

                Err e ->
                    if str == newSkillOption then
                        { model | newUserSkillId = Nothing } ! []
                    else
                        Debug.log e model ! []

        ChangeNewSkillName str ->
            if String.length str /= 0 then
                { model | newSkill = Just { skillId = Nothing, skillName = str } } ! []
            else
                { model | newSkill = Nothing } ! []

        AssociateSkill ->
            case model.newUserSkillId of
                Just sid ->
                    model ! [ associateSkill model.user sid model.newSkillPercentage ]

                Nothing ->
                    case model.newSkill of
                        Just newSkill ->
                            model ! [ createUserSkill model.user newSkill model.newSkillPercentage ]

                        Nothing ->
                            model ! []

        GotNewSkill res ->
            case res of
                Ok newUserSkillId ->
                    case newUserSkillId of
                        Just skill ->
                            (\m -> { m | newUserSkillId = Maybe.andThen .skillId <| List.head m.skills })
                                { model
                                    | usersSkills = skill :: model.usersSkills
                                    , skills =
                                        List.filter (\s -> s.skillId /= skill.usersSkill.skillId) model.skills
                                }
                                ! []

                        Nothing ->
                            Debug.log "Got Nothing for new skill!!!" model ! []

                Err e ->
                    Debug.log (toString e) model ! []

        DeleteSkill swp ->
            case swp.usersSkill.skillId of
                Just sid ->
                    { model | skills = swp.usersSkill :: model.skills, newUserSkillId = swp.usersSkill.skillId }
                        ! [ deleteUserSkill model.user sid ]

                Nothing ->
                    model ! []

        DeletedSkill sid (Ok True) ->
            { model | usersSkills = List.filter (\swp -> swp.usersSkill.skillId /= Just sid) model.usersSkills } ! []

        DeletedSkill sid (Ok False) ->
            Debug.log ("Tried to delete user's skill with id " ++ toString sid ++ ", but didn't succeed.") model ! []

        DeletedSkill _ (Err e) ->
            Debug.log (toString e) model ! []



-- commands


{-| Sends a post request to /api/forUser/{user_id}/{skill_id} with the given percentage in the request body.
-}
associateSkill : PublicUser -> SkillId -> Percentage -> Cmd Msg
associateSkill currentUser sid perc =
    Http.send GotNewSkill <| postApiSkillsForUserByUserIdBySkillId currentUser.publicUserId sid perc


{-| Takes a user and skill id, sends a delete request to /api/forUser/{uid}/{skill id}.
-}
deleteUserSkill : PublicUser -> SkillId -> Cmd Msg
deleteUserSkill currentUser sid =
    Http.send (DeletedSkill sid) <| deleteApiSkillsForUserByUserIdBySkillId currentUser.publicUserId sid


{-| Takes a user, skill, and percentage. A post request to /api/forUser/{user_id} with the new skill and the percentage.
-}
createUserSkill : PublicUser -> Skill -> Percentage -> Cmd Msg
createUserSkill currentUser newSkill perc =
    Http.send GotNewSkill <|
        postApiSkillsForUserByUserId currentUser.publicUserId <|
            { newUserSkillSkill = newSkill
            , newUserSkillPercentage = perc
            }



-- view


{-| Shows a table with all user's skills.
-}
view : Model -> Html Msg
view model =
    let
        user =
            model.user

        availableSkills =
            List.filter (\skill -> not <| List.member skill.skillId <| List.map (\swp -> swp.usersSkill.skillId) model.usersSkills) model.skills
    in
        div []
            [ Table.table
                { options = [ Table.bordered ]
                , thead =
                    Table.thead [ Table.defaultHead ]
                        [ Table.tr []
                            [ Table.th [] [ text "Skill" ]
                            , Table.th [] [ text "Percentage" ]
                            , Table.th [] [ text "" ]
                            ]
                        ]
                , tbody =
                    Table.tbody [] <|
                        List.map
                            (\swp ->
                                Table.tr []
                                    [ Table.td [] [ text swp.usersSkill.skillName ]
                                    , Table.td [] [ text <| toString swp.skillPercentage ]
                                    , Table.td []
                                        [ Button.linkButton [ Button.attrs [ onClick <| DeleteSkill swp ] ] [ text "Delete" ]
                                        ]
                                    ]
                            )
                            model.usersSkills
                }
            , Form.form [ onSubmit AssociateSkill ] <|
                [ Form.label [] [ text "Skill" ]
                , Select.select [ Select.onChange ChangeSkill ] <|
                    List.map (\skill -> Select.item [ value <| toString skill.skillId ] [ text skill.skillName ]) availableSkills
                        ++ [ Select.item [ value newSkillOption ] [ text "New Skill" ] ]
                ]
                    ++ (if model.newUserSkillId == Nothing then
                            [ Form.label [] [ text "New Skill Name" ]
                            , Input.text [ Input.value <| Maybe.withDefault "" <| Maybe.map .skillName model.newSkill, Input.onInput ChangeNewSkillName ]
                            ]
                        else
                            []
                       )
                    ++ [ Form.label [] [ text "Percentage" ]
                       , Input.number [ Input.value <| toString model.newSkillPercentage, Input.onInput ChangePercentage ]
                       , input [ type_ "submit", value "Add", class "btn btn-primary" ] []
                       ]
            ]
