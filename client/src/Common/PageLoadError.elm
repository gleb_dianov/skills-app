module Common.PageLoadError exposing (..)

{-| Module for showing page loading errors.

# Model
@docs Model, PageLoadError

# Converting to PageLoadError

@docs pageLoadError

# Showing the error

@docs view

-}

import Html exposing (..)

{-| Just an error message.
-}
type alias Model =
    { errorMessage : String
    }

{-| Wrapper for the Model
-}
type PageLoadError = PageLoadError Model

{-| Takes a string an converts it to a PageLoadError.

    pageLoadError "example"
    --> PageLoadError { errorMessage = "example" }
-}
pageLoadError : String -> PageLoadError
pageLoadError errorMessage = PageLoadError { errorMessage = errorMessage }

{-| Takes a page load error and just outputs the error message.
-}
view : PageLoadError -> Html msg
view (PageLoadError model) = text model.errorMessage
