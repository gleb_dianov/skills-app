module Common.Maybe exposing(..)

{-| This modules exports helpers for working with the maybe data type.

# Null checking

@docs isJust

-}

{-| Checks if the given maybe function has a value.

    isJust (Just 5)
    --> True

    isJust Nothing
    --> False
-}
isJust : Maybe a -> Bool
isJust x =
    case x of
        Just _ -> True
        Nothing -> False
