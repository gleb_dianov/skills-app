module Common.Date exposing (..)

{-| This module is gives a date json decoder for the format that Haskell uses.


# Date decoding

@docs decodeDate


# Date encoding

@docs dateToString

-}

import Date exposing (..)
import Json.Decode exposing (..)


{-| A date decoder for json in the Haskell's format.
-}
decodeDate : Decoder Date
decodeDate =
    string
        |> andThen
            (\val ->
                case fromString val of
                    Err err ->
                        fail err

                    Ok date ->
                        succeed date
            )


{-| Takes a date and converts to "yyyy-mm-dd" format.
-}
dateToString : Date -> String
dateToString date =
    let
        monthToNum month =
            case month of
                Jan ->
                    "01"

                Feb ->
                    "02"

                Mar ->
                    "03"

                Apr ->
                    "04"

                May ->
                    "05"

                Jun ->
                    "06"

                Jul ->
                    "07"

                Aug ->
                    "08"

                Sep ->
                    "09"

                Oct ->
                    "10"

                Nov ->
                    "11"

                Dec ->
                    "12"
    in
        toString (year date)
            ++ "-"
            ++ (monthToNum <| month date)
            ++ "-"
            ++ if day date < 10 then "0" ++ toString (day date) else toString (day date)
