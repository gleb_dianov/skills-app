module Main exposing (..)

{-| Docs


# Model

@docs SubModel, Model


# Routing

@docs handleRoute


# Initializing

@docs init


# Updating

@docs Msg, update


# Subscriptions

@docs subscriptions


# Viewing

@docs navbarLink, navbar, viewLink, viewPage, view


# Main

@docs main

-}

import Navigation exposing (Location)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Bootstrap.Navbar as Navbar
import Bootstrap.CDN exposing (stylesheet)
import Task
import Http
import Json.Decode as Decode
import Generated.Api exposing (..)
import Routes
import Page.Home as Home
import Page.Profile as Profile
import Page.Login as Login
import Page.Admin as Admin
import Common.PageLoadError as PageLoadError exposing (PageLoadError(..), pageLoadError)
import Common.Maybe exposing (..)
import Page.SignUp as SignUp


-- model


{-| This data type is used for storing the model of a page.
-}
type SubModel
    = HomeModel Home.Model
    | ProfileModel Profile.Model
    | AdminModel Admin.Model
    | LoginModel Login.Model
    | SignUpModel SignUp.Model
    | ErrPage String
    | NotFound
    | Blank


{-| Contains the current route,
whether state of the page (ready or not),
current user if s/he is logged in,
sub model of the current page,
state of the navbar.
-}
type alias Model =
    { route : Routes.Route
    , ready : Bool
    , currentUser : Maybe PublicUser
    , subModel : SubModel
    , navbarState : Navbar.State
    }



-- msg


{-| Represents one of the actions on the page:
route changed, a route loaded, got current user's details,
a message from the home page, a message from the profile page,
a message from the log in page, a message from the sign up page,
a page loaded, a message from the navbar.
-}
type Msg
    = RouteTo Routes.Route
    | RouteChanged Routes.Route
    | GotUserInfo (Result Http.Error PublicUser)
    | HomeMsg Home.Msg
    | ProfileMsg Profile.Msg
    | AdminMsg Admin.Msg
    | LoginMsg Login.Msg
    | SignUpMsg SignUp.Msg
    | Loaded (Result PageLoadError SubModel)
    | NavbarMsg Navbar.State



-- init


{-| Takes the current route and returns the initial model and commands
to load the right page and get current user's info.
-}
init : Navigation.Location -> ( Model, Cmd Msg )
init location =
    let
        ( navbarState, navbarCmd ) =
            Navbar.initialState NavbarMsg
    in
        { route = Routes.fromLocation location
        , ready = False
        , currentUser = Nothing
        , subModel = Blank
        , navbarState = navbarState
        }
            ! [ Http.send GotUserInfo <| getApiUsersMe, navbarCmd ]


{-| Takes the new route and the current model, returns model with the route set to the new route and
the state of the page set to loading. Then sends a new command that loads the required page.
If the user is not logged in then the function will not let him open the profile or the admin page.
-}
handleRoute : Routes.Route -> Model -> ( Model, Cmd Msg )
handleRoute route ({ ready } as m) =
    let
        model =
            { m | route = route, ready = False }
    in
        case route of
            Routes.Home ->
                model ! [ Task.attempt (Result.map HomeModel >> Loaded) Home.init ]

            Routes.Profile ->
                model
                    ! case model.currentUser of
                        Nothing ->
                            [ Routes.navigateTo Routes.LogIn ]

                        Just user ->
                            [ Task.attempt (Result.map ProfileModel >> Loaded) <| Profile.init user ]

            Routes.Admin ->
                model
                    ! case model.currentUser of
                        Nothing ->
                            [ Routes.navigateTo Routes.LogIn ]

                        Just user ->
                            -- if user.publicUserIsAdmin then
                            [ Task.attempt (Result.map AdminModel >> Loaded) <| Admin.init user ]

            -- else
            --     [ Debug.log "Not an admin!!!" <| Routes.navigateTo Routes.Home ]
            Routes.LogIn ->
                model
                    ! if isJust model.currentUser then
                        [ Task.attempt (Result.map HomeModel >> Loaded) Home.init ]
                      else
                        [ Task.attempt (Result.map LoginModel >> Loaded) Login.init ]

            Routes.SignUp ->
                model ! [ Task.attempt (Result.map SignUpModel >> Loaded) SignUp.init ]

            Routes.NotFound ->
                model ! []



-- update


{-| If gets a message from the opened page then updates the page and sends the commands.
When a routing message is passed changes the route to the requested.
When gets user info from the server returns the new model with the current user.
If the user info request returns an error then sends a navigation to the login page command.
If login or sign up page gives message saying that the user signed in/up then it requests
user details from the server.
-}
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        toPageMsg toModel toMsg subUpdate subMsg subModel =
            let
                ( newModel, newCmd ) =
                    subUpdate subMsg subModel
            in
                ( { model | subModel = toModel newModel }
                , Cmd.map toMsg newCmd
                )
    in
        case ( msg, model.subModel ) of
            ( RouteChanged route, _ ) ->
                handleRoute route model

            ( RouteTo route, _ ) ->
                model ! [ Routes.navigateTo route ]

            ( HomeMsg subMsg, HomeModel subModel ) ->
                toPageMsg HomeModel HomeMsg Home.update subMsg subModel

            ( ProfileMsg subMsg, ProfileModel subModel ) ->
                toPageMsg ProfileModel ProfileMsg Profile.update subMsg subModel

            ( AdminMsg subMsg, AdminModel subModel ) ->
                toPageMsg AdminModel AdminMsg Admin.update subMsg subModel

            ( LoginMsg (Login.LoggedIn (Ok ())), LoginModel subModel ) ->
                model ! [ Http.send GotUserInfo <| getApiUsersMe, Routes.navigateTo Routes.Home ]

            ( LoginMsg subMsg, LoginModel subModel ) ->
                toPageMsg LoginModel LoginMsg Login.update subMsg subModel

            ( SignUpMsg (SignUp.SignedUp (Ok ())), SignUpModel subModel ) ->
                model ! [ Http.send GotUserInfo <| getApiUsersMe, Routes.navigateTo Routes.Home ]

            ( SignUpMsg subMsg, SignUpModel subModel ) ->
                toPageMsg SignUpModel SignUpMsg SignUp.update subMsg subModel

            ( _, NotFound ) ->
                ( model, Cmd.none )

            ( GotUserInfo (Ok user), _ ) ->
                handleRoute model.route { model | currentUser = Just user }

            ( GotUserInfo (Err err), _ ) ->
                model
                    ! if model.route /= Routes.SignUp then
                        [ Routes.navigateTo Routes.LogIn ]
                      else
                        [ Routes.navigateTo Routes.SignUp ]

            ( Loaded (Ok newModel), _ ) ->
                { model | ready = True, subModel = newModel } ! []

            ( Loaded (Err err), _ ) ->
                { model | ready = True, subModel = ErrPage <| toString err } ! []

            ( NavbarMsg state, _ ) ->
                { model | navbarState = state } ! []

            _ ->
                model ! []



-- subscriptions


{-| Adds subscription to the navbar to update it when something happens to it.
-}
subscriptions : Model -> Sub Msg
subscriptions model =
    Navbar.subscriptions model.navbarState NavbarMsg



-- view


{-| If the user is logged in then the requested page is shown.
If the user is not logged in and the sign in/up page is requested then the page is shown, otherwise an error message.
-}
viewPage : Model -> Html Msg
viewPage model =
    case model.currentUser of
        Nothing ->
            case model.subModel of
                LoginModel loginModel ->
                    Login.view loginModel |> Html.map LoginMsg

                SignUpModel signUpModel ->
                    SignUp.view signUpModel |> Html.map SignUpMsg

                Blank ->
                    text ""

                ErrPage err ->
                    PageLoadError.view <| PageLoadError { errorMessage = err }

                _ ->
                    PageLoadError.view <|
                        PageLoadError { errorMessage = "The impossible happened, unauthorized user!!!" }

        Just currentUser ->
            case model.subModel of
                NotFound ->
                    text "404 NOT FOUND!!!"

                HomeModel homeModel ->
                    Home.view homeModel |> Html.map HomeMsg

                ProfileModel profileModel ->
                    Profile.view profileModel |> Html.map ProfileMsg

                AdminModel adminModel ->
                    Admin.view adminModel |> Html.map AdminMsg

                LoginModel loginModel ->
                    PageLoadError.view <|
                        PageLoadError
                            { errorMessage =
                                "The impossible happened, authorized user on the login page!!!"
                            }

                SignUpModel signUpModel ->
                    PageLoadError.view <|
                        PageLoadError
                            { errorMessage =
                                "The impossible happened, authorized user on the sign up page!!!"
                            }

                ErrPage err ->
                    PageLoadError.view <| PageLoadError { errorMessage = err }

                Blank ->
                    text ""


{-| Shows all the site navigation in a nice navbar.
-}
navbar : Model -> Html Msg
navbar model =
    Navbar.config NavbarMsg
        |> Navbar.withAnimation
        |> Navbar.inverse
        |> Navbar.collapseMedium
        |> Navbar.brand [ href "#" ] [ text "Skills App" ]
        |> Navbar.items
            (if model.currentUser == Nothing then
                [ navbarLink Routes.LogIn
                , navbarLink Routes.SignUp
                ]
             else
                (if Maybe.withDefault False <| Maybe.map .publicUserIsAdmin model.currentUser then
                    [ navbarLink Routes.Admin ]
                 else
                    []
                )
                    ++ [ navbarLink Routes.Home
                       , navbarLink Routes.Profile
                       , Navbar.itemLink [ href "/api/logout" ] [ text "Log Out" ]
                       ]
            )
        |> Navbar.view model.navbarState


{-| Showing a link for the navbar.
-}
navbarLink : Routes.Route -> Navbar.Item Msg
navbarLink route =
    let
        options =
            { preventDefault = True, stopPropagation = True }
    in
        Navbar.itemLink [ href <| Routes.toString route, onWithOptions "click" options <| Decode.succeed <| RouteTo route ]
            [ text <| Routes.toName route ]


{-| Takes a route and returns an html link to the page with it's name.
-}
viewLink : Routes.Route -> Html Msg
viewLink route =
    let
        options =
            { preventDefault = True, stopPropagation = True }
    in
        a
            [ href <| Routes.toString route
            , onWithOptions "click" options <| Decode.succeed <| RouteTo route
            ]
            [ text <| Routes.toName route ]


{-| Imports the stylesheet, shows the navbar and the current page.
-}
view : Model -> Html Msg
view model =
    div []
        [ stylesheet
        , navbar model
        , br [] []
        , viewPage model
        ]


{-| Takes route and renders the application.
-}
main : Program Never Model Msg
main =
    Navigation.program (Routes.fromLocation >> RouteChanged)
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }
