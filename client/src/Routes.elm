module Routes exposing (..)

{-| This module contains functions and a data type for working with different routes of the app.


# Routes

@docs Route, index, home, login, profile, admin, signUp, routes


# Functions for working with routes

@docs match, toString, toName, fromLocation, navigateTo

-}

import Route exposing ((:=), static, router, reverse)
import Navigation exposing (Location)


{-| Represents a page of the app.
-}
type Route
    = Home
    | Profile
    | Admin
    | LogIn
    | SignUp
    | NotFound


{-| Assignes url '/' to the home page.
-}
index : Route.Route Route
index =
    Home := static ""


{-| Assignes url '/home' to the home page.
-}
home : Route.Route Route
home =
    Home := static "home"


{-| Assigns url '/login' to the login page.
-}
login : Route.Route Route
login =
    LogIn := static "login"


{-| Assigns url '/profile' to the profile page.
-}
profile : Route.Route Route
profile =
    Profile := static "profile"


{-| Assigns url '/sign_up' to the sign up page.
-}
signUp : Route.Route Route
signUp =
    SignUp := static "sign_up"


{-| Assigns url '/admin' to the admin page.
-}
admin : Route.Route Route
admin =
    Admin := static "admin"


{-| Creates a router for the app navigation.
-}
routes : Route.Router Route
routes =
    router [ index, home, profile, admin, login, signUp ]


{-| Takes a url and tries to find the corresponding page.
-}
match : String -> Maybe Route
match =
    Route.match routes


{-| Converts a route to the corresponding url.
-}
toString : Route -> String
toString route =
    case route of
        Home ->
            reverse home []

        Profile ->
            reverse profile []

        Admin ->
            reverse admin []

        LogIn ->
            reverse login []

        SignUp ->
            reverse signUp []

        NotFound ->
            Debug.crash "cannot route to NotFound"


{-| Converts the given location to a route.
-}
fromLocation : Location -> Route
fromLocation location =
    Maybe.withDefault NotFound <| match location.pathname


{-| Takes a route and returns the name of the page.
-}
toName : Route -> String
toName route =
    case route of
        Home ->
            "Home"

        Profile ->
            "Profile"

        Admin ->
            "Admin"

        SignUp ->
            "Sign Up"

        LogIn ->
            "Log In"

        NotFound ->
            "404 Page Not Found"


{-| Takes a route and sends command that navigates to that route.
-}
navigateTo : Route -> Cmd msg
navigateTo =
    toString >> Navigation.newUrl
