{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

{-|
Module      : Routes.Public.API
Description : Public API routes.
-}

module Routes.Public.API where

import           Control.Monad.Trans
import           Database.PostgreSQL.Simple              (Connection)
import           Servant
import           Servant.Server.Experimental.Auth.Cookie

import           Common.Authentication
import           Common.Html
import           Model.User

{-|
Routes:
/login   - takes login details from the request body and authenticates the user.
/sign_up - takes details from the request body, creates a new user, and logs in.
-}
type API = "login"   :> ReqBody '[JSON] Login   :> PostNoContent '[JSON] (Cookied NoContent)
      :<|> "sign_up" :> ReqBody '[JSON] NewUser :> PostNoContent '[JSON] (Cookied NoContent)
      :<|> "logout" :> Get '[HTML] (Headers '[Header "Set-Cookie" EncryptedSession] Html)

-- | Takes a connection, a persistent server key, authentication cookie settings and random source.
-- Implements the API.
server :: Connection -> PersistentServerKey -> AuthCookieSettings -> RandomSource -> Server API
server conn key cookieSettings randomSource = authenticate conn key cookieSettings randomSource :<|> signUp :<|> logOut
    where logOut = serveIndexHtml >>= removeSession cookieSettings
          signUp newUser =
            liftIO (createUser conn newUser)
              >>= maybe (throwError err400) (const $ authenticate conn key cookieSettings randomSource $ toLogin newUser)
