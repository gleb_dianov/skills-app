{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

{-|
Module      : Routes.Public
Description : Public routes of the web app.
-}

module Routes.Public where

import           Database.PostgreSQL.Simple              (Connection)
import           Servant
import           Servant.Server.Experimental.Auth.Cookie

import           Common.Html
import qualified Routes.Public.API                       as API

{-|
Routes:
/, /login, /sign_up, /home, /profile, /admin - return the index.html as it's a single page application and the client side will show the right page.
/assets/{name} - returns file ./assets/{name}
/api/... - public api
-}
type Routes = Get '[HTML] Html
         :<|> "login" :> Get '[HTML] Html
         :<|> "sign_up" :> Get '[HTML] Html
         :<|> "home" :> Get '[HTML] Html
         :<|> "profile" :> Get '[HTML] Html
         :<|> "admin" :> Get '[HTML] Html
         :<|> "assets" :> Raw
         :<|> "api" :> API.API

-- | Takes a connection to the database, persistent server key, authentication cookie settings, and random source.
-- Implements the routes.
server :: Connection -> PersistentServerKey -> AuthCookieSettings -> RandomSource -> Server Routes
server conn key acs rs = serveIndexHtml
                    :<|> serveIndexHtml
                    :<|> serveIndexHtml
                    :<|> serveIndexHtml
                    :<|> serveIndexHtml
                    :<|> serveIndexHtml
                    :<|> serveDirectoryWebApp "./assets"
                    :<|> API.server conn key acs rs
