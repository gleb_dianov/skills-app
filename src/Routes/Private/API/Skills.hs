{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

{-|
Module      : Routes.Private.API.Skills
Desctiption : API routes for working with skills.
-}

module Routes.Private.API.Skills where

import           Control.Lens
import           Control.Monad.Trans
import           Data.Int                   (Int64)
import           Database.PostgreSQL.Simple (Connection)
import           Prelude                    hiding (id)
import           Servant

import           Common.Database
import           Model.Skill                hiding (id)
import           Model.User
import           Model.UserSkill            (NewUserSkill, SkillId,
                                             SkillWithPercentageRead, UserId,
                                             associateSkill, createUserSkill,
                                             deleteUserSkill,
                                             getUserSkillsWithUserId)

-- | Throws error 403 with message "Permission Denied!"
notAllowed :: Handler a
notAllowed = throwError err403 { errBody = "Permission Denied!" }

{-|
Get:    /           - returns all users's skills
Post:   /           - takes a new user's skill, saves it, and returns the new skill with percentage
Post:   /{skill_id} - takes percentage from the request body, validates it, and associates with the user
Delete: /{skill_id} - removes a user's skill
-}
type UserRoutes = Get '[JSON] [SkillWithPercentageRead]
             :<|> ReqBody '[JSON] NewUserSkill :> Post '[JSON] (Maybe SkillWithPercentageRead)
             :<|> Capture "skillId" SkillId :> ( ReqBody '[JSON] Int64 :> Post '[JSON] (Maybe SkillWithPercentageRead)
                                            :<|> Delete '[JSON] Bool
                                               )


-- | Takes a connection to the database, requesting user, a user id, and implements UserRoutes.
forUser :: Connection -> UserRead -> Id -> Server UserRoutes
forUser conn currentUser uid =
    if currentUser ^. id == uid || currentUser ^. isAdmin
       then
         liftIO (getUserSkillsWithUserId conn uid) :<|> liftIO . createUserSkill conn uid :<|> (\sid -> liftIO . associateSkill conn uid sid :<|> liftIO (deleteUserSkill conn uid sid))
       else notAllowed :<|> const notAllowed :<|> const (const notAllowed :<|> notAllowed)

{-| Routes:
Get: /                   - returns all skills
Get: /for_user/{user_id} - UserRoutes routes
-}
type API = Get '[JSON] [SkillRead]
      :<|> "forUser" :> Capture "userId" UserId :> UserRoutes

-- | Takes a connection to the database, requesting user and implements the API
server :: Connection -> UserRead -> Server API
server conn currentUser = liftIO (getSkills conn) :<|> forUser conn currentUser
