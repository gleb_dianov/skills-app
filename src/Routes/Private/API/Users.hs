{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

{-|
Module      : Routes.Private.API.Users
Description : API routes for working with users.
-}

module Routes.Private.API.Users where

import           Control.Lens
import           Control.Monad.Trans
import           Data.Int                                (Int64)
import           Database.PostgreSQL.Simple              (Connection)
import           Prelude                                 hiding (id)
import           Servant

import           Common.Database
import           Common.Handler
import           Model.User


{-|
Get:    /       - returns all users
Put:    /       - updates a user with data from the request if the requesting user is an admin or the user who gets updated
Delete: /       - removes user
Get:    /logout - logs user out
Get:    /me     - returns details of the requesting user
Get:    /{uid}  - returns the user with the uid if the requesting user is an admin or s/he's id is the uid
-}
type API = "deleteAccount" :> Get '[JSON] Bool
      :<|> Get '[JSON] [PublicUser]
      :<|> ReqBody '[JSON] PublicUser :> Put '[JSON] PublicUser
      :<|> "me" :> Get '[JSON] PublicUser
      :<|> Capture "userId" Int64 :> Get '[JSON] PublicUser


-- | Takes a connection to the database and the requesting user, implements the API.
server :: Connection -> UserRead -> Server API
server conn currentUser
      = (liftIO (deleteUserWithId conn $ currentUser ^. id)
          >>= \res -> if res then redirect303 "/api/logout" else throwError err500 { errBody = "Something went wrong..." })
   :<|> (if currentUser ^. isAdmin then returnAllUsers else throw403)
   :<|> updateUser
   :<|> return (toPublicUser currentUser)
   :<|> userServer
   where maybe404 = maybe $ throwError err404 { errBody = "Bad request, user with this uid doesn't exist!" }
         throw403 = throwError err403 { errBody = "Permission denied!" }
         updateUser :: PublicUser -> Handler PublicUser
         updateUser changedUser = if currentUser ^. isAdmin || currentUser ^. id == changedUser ^. id
                                     then liftIO (updateUserWithId conn changedUser) >>= maybe404 (return . toPublicUser)
                                     else throw403
         userServer :: Id -> Handler PublicUser
         userServer uid = if currentUser ^. isAdmin || currentUser ^. id == uid then getUser else throw403
             where getUser :: Handler PublicUser
                   getUser = liftIO (getUserWithId conn uid) >>= maybe404 (return . toPublicUser)
         returnAllUsers = if currentUser ^. isAdmin then liftIO (fmap toPublicUser <$> getUsers conn) else throw403

