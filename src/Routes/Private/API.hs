{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

{-|
Module      : Routes.Private.API
Description : API routes available for authorised users only.
-}

module Routes.Private.API where

import           Control.Lens
import           Control.Monad.Trans
import           Database.PostgreSQL.Simple              (Connection)
import           Servant

import           Model.AdminPost
import           Model.User
import qualified Routes.Private.API.Skills               as Skills
import qualified Routes.Private.API.Users                as Users


{-|
... : /users/...   - users api
Get : /admin_posts - returns all admin posts
Post: /admin_post  -
-}
type API = "users" :> Users.API
      :<|> "adminPosts" :> Get '[JSON] [AdminPostRead]
      :<|> "adminPost" :> ReqBody '[JSON] NewAdminPost :> Post '[JSON] (Maybe AdminPostRead)
      :<|> "skills" :> Skills.API

-- | Takes a connection to the database and the requesting user, implements the API.
server :: Connection -> UserRead -> Server API
server conn currentUser
       = Users.server conn currentUser
    :<|> liftIO (getAdminPosts conn)
    :<|> postAdminPost
    :<|> Skills.server conn currentUser
    where postAdminPost :: NewAdminPost -> Handler (Maybe AdminPostRead)
          postAdminPost post = if currentUser ^. isAdmin then liftIO $ createAdminPost conn post
                                                         else throwError err403 { errBody = "Permission Denied!" }
