{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE ExplicitForAll        #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TypeOperators         #-}

{-|
Module      : Server
Description : The main module that puts all the parts of the web app together.
-}

module Server where

import           Crypto.Cipher.AES                       (AES256)
import           Crypto.Cipher.Types                     (ctrCombine)
import           Crypto.Hash.Algorithms                  (SHA256)
import           Data.Proxy
import           Database.PostgreSQL.Simple              (Connection)
import           Network.Wai
import           Servant                                 hiding (Secure)
import           Servant.Server.Experimental.Auth
import           Servant.Server.Experimental.Auth.Cookie

import           Common.Authentication
import           Model.User
import qualified Routes.Private.API                      as Private.API
import qualified Routes.Public                           as Public

-- | Variable for GenerateElm.hs
-- Allows us to use API type as a value.
apiForClientSide :: Proxy ("api" :> Private.API.API)
apiForClientSide = Proxy

-- | Type synonym for protecting paths
type AppAuth = AuthProtect "cookie-auth"

-- | Public routes and protected private routes.
type Routes = Public.Routes
         :<|> AppAuth :> "api" :> Private.API.API

-- | Variable for using the Routes type as a value.
routes :: Proxy Routes
routes = Proxy

-- | Takes a connection, persistent server key, authentication cookie settings and random source.
-- Handles all the routes of the application.
server :: Connection -> PersistentServerKey -> AuthCookieSettings -> RandomSource -> Server Routes
server conn key acs rs = Public.server conn key acs rs :<|> Private.API.server conn . wmData

-- | Authentication context type
type AuthenticationContext = AuthHandler Request (WithMetadata UserRead)

-- | Authentication cookie settings
cookieSettings :: AuthCookieSettings
cookieSettings =
    AuthCookieSettings
        { acsSessionField     = "Session"
        , acsCookieFlags      = ["HttpOnly", "Secure"]
        , acsMaxAge           = fromIntegral (6 * 3600 :: Integer)
        , acsExpirationFormat = "%0Y%m%d%H%M%S"
        , acsPath             = "/"
        , acsHashAlgorithm    = Proxy :: Proxy SHA256
        , acsCipher           = Proxy :: Proxy AES256
        , acsEncryptAlgorithm = ctrCombine
        , acsDecryptAlgorithm = ctrCombine
        }

-- | Authentication Context
contexts :: Connection -> PersistentServerKey -> Context '[AuthenticationContext]
contexts conn serverKey = (authHandler conn cookieSettings serverKey :: AuthenticationContext) :. EmptyContext

-- | Takes a connection to the database, a persistent server key, and a random source.
-- Puts together all the authentication cookie settings and the server and returns the application.
app :: Connection -> PersistentServerKey -> RandomSource -> Application
app conn sk rs = serveWithContext routes (contexts conn sk) $ server conn sk cookieSettings rs
