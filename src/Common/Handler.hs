{-# LANGUAGE OverloadedStrings #-}

module Common.Handler where

import           Control.Monad.Trans.Except
import           Data.ByteString            as BS
import           Servant

-- | Takes a route and redirects to it.
redirect303 :: BS.ByteString -> Handler a
redirect303 url = Handler $ throwE err303 { errHeaders = [("Location", url)]}

