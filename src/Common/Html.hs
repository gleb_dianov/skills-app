{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}

{-|
Module      : Common.Html
Description : Helpers for working with HTML and Servant
-}

module Common.Html where

import           Control.Monad.Trans
import qualified Data.ByteString.Lazy as BL
import           Network.HTTP.Media   ((//), (/:))
import           Servant

-- | HTML data type
data HTML

-- | newtype for lazy bytestring
newtype Html = Html { unHtml :: BL.ByteString }

-- | Accept header for HTML
instance Accept HTML where
    contentType _ = "text" // "html" /: ("charset", "utf-8")

-- | Servant requires this instance
instance MimeRender HTML Html where
    mimeRender _ = unHtml

-- | Servant requires this instance
instance MimeUnrender HTML Html where
    mimeUnrender _ = Right . Html

-- | My elm code gets compiled into html file ./assets/index.html
-- This handler returns content of that file.
serveIndexHtml :: Handler Html
serveIndexHtml = liftIO (Html <$> BL.readFile "./assets/index.html")
