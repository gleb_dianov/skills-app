{-# LANGUAGE Arrows #-}

{-|
Module      : Common.Database
Description : Helpers for working with the database.
-}

module Common.Database where

import           Control.Arrow
import           Data.Int      (Int64)
import           Opaleye

-- | Converts a function that takes anything and returns postgres boolean
-- to a query that filters by this function.
filterSql :: (a -> Column PGBool) -> QueryArr a a
filterSql f = proc x -> do
  restrict -< f x
  returnA  -<   x

-- | Type synonym for Int64.
type Id = Int64
