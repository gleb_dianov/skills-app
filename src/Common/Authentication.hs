{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies      #-}

{-|
Module      : Common.Authentication
Description : Helpers for authenticating users.
-}

module Common.Authentication where

import           Control.Lens
import           Control.Monad.Catch                     (catch)
import           Control.Monad.Extra
import           Control.Monad.Trans
import           Control.Monad.Trans.Except
import           Crypto.PasswordStore
import           Data.Aeson
import           Data.ByteString                         as BS
import qualified Data.ByteString.Char8                   as BSC8
import qualified Data.ByteString.Lazy                    as BL
import           Data.Maybe
import           Data.Serialize
import           Data.Text                               as T
import           Data.Text.Encoding
import           Database.PostgreSQL.Simple
import           GHC.Generics
import           Network.Wai
import           Prelude                                 hiding (id)
import           Servant
import           Servant.Server.Experimental.Auth
import           Servant.Server.Experimental.Auth.Cookie

import           Model.User

-- | Represents login details.
data Login = Login
    { loginEmail    :: Text
    , loginPassword :: Text
    } deriving (Eq, Show, Read, Generic, ToJSON, FromJSON)

-- | Need this to convert to cookies
instance Serialize Text where
    put txt = (putWord32le . fromIntegral . BS.length) bytes >> putByteString bytes
        where bytes = encodeUtf8 txt
    get = getWord32le >>= fmap decodeUtf8 . getBytes . fromIntegral

-- | Converts NewUser to Login.
toLogin :: NewUser -> Login
toLogin user = Login { loginEmail = newUserEmail user
                     , loginPassword = newUserPassword user
                     }

type instance AuthCookieData = UserRead

-- | Takes a connection to the database, authentication cookie settings and a server key set.
-- Returns an authentication handler that responds to requests with errors if there are no cookies or if they are invalid.
authHandler :: ServerKeySet sks => Connection -> AuthCookieSettings -> sks -> AuthHandler Request (WithMetadata UserRead)
authHandler conn acs sks = mkAuthHandler $ \request ->
    getSession acs sks request `catch` handleEx >>= maybe (throwError err403 { errBody = "No cookies"     }) fromCookie
                                                >>= maybe (throwError err403 { errBody = "Invalid cookie" }) return
    where handleEx :: AuthCookieException -> Handler (Maybe (WithMetadata Email))
          handleEx ex = throwError err403 { errBody = BL.fromStrict $ BSC8.pack $ show ex }
          fromCookie wm = fmap (flip WithMetadata $ wmRenew wm) <$> liftIO (getUserWithEmail conn $ wmData wm)

-- | Takes a connection to the database, persistent server key, authentication cookie settings, random source, and login details.
-- Finds the user with the given email and checks that passwords match.
-- If a user with this email doesn't exist or password don't match it responds with an error.
-- Otherwise it sends a response with no content and a header with a cookie.
authenticate :: Connection -> PersistentServerKey -> AuthCookieSettings -> RandomSource -> Login -> Handler (Cookied NoContent)
authenticate conn key cookieSettings randomSource (Login uEmail password) =
    Handler $ ifM (check password <$> user) (toNoContent key =<< fromJust <$> user) $ throwError err401
    where user :: ExceptT ServantErr IO (Maybe UserRead)
          user = liftIO $ getUserWithEmail conn uEmail

          toNoContent :: PersistentServerKey -> UserRead -> ExceptT ServantErr IO (Cookied NoContent)
          toNoContent key usr = addSession cookieSettings randomSource key (usr ^. email) NoContent

          check :: Text -> Maybe UserRead -> Bool
          check psswd usr
            | isNothing usr = False
            | passwordsMatch = True
            | otherwise = False
            where passwordsMatch = encryptPassword psswd (importSalt $ fromJust usr ^. salt) == fromJust usr ^. passHash
