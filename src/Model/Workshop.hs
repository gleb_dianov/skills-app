{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE TemplateHaskell        #-}

{-|
Module      : Model.Workshop
Description : Module for working with workshops.
-}

module Model.Workshop where

import           Control.Lens.TH
import           Data.Aeson
import           Data.Profunctor.Product.TH
import           Data.Text                  (Text)
import           GHC.Generics
import           Opaleye
import           Servant.Elm

import           Common.Database

-- | Models a workshop with variable types of fields.
data Workshop id name = Workshop
    { workshopId   :: id   -- ^ workshop id
    , workshopName :: name -- ^ workshop name
    } deriving (Generic, ToJSON, FromJSON, Show, ElmType)

-- | Type for writing workshops to the PostgreSQL database
-- id is Maybe so that PostgreSQL can set the right id
type WorkshopWriteColumns = Workshop (Maybe (Column PGInt8)) (Column PGText)

-- | Type for reading workshops from the PostgreSQL database
type WorkshopReadColumns = Workshop (Column PGInt8) (Column PGText)

-- | Type for working with workshops to Haskell before writing them to the database.
type WorkshopWrite = Workshop (Maybe Id) Text

-- | Type for working with workshops to Haskell after reading them from the database.
type WorkshopRead = Workshop Id Text

-- Generating product-profunctor Default instances for Workshop
makeAdaptorAndInstance "pWorkshop" ''Workshop

-- Generating lenses using camelCaseFields rules (https://hackage.haskell.org/package/lens-4.15.3/docs/Control-Lens-TH.html)
makeLensesWith camelCaseFields ''Workshop

-- | Table that connects the "workshops" table from the database to Haskell
workshopTable :: Table WorkshopWriteColumns WorkshopReadColumns
workshopTable = Table "workshops" $ pWorkshop Workshop
    { workshopId = optional "id"
    , workshopName = required "name"
    }
