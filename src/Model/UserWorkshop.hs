{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE TemplateHaskell        #-}

module Model.UserWorkshop where

import           GHC.Generics

import           Data.Aeson
import           Data.Int                   (Int64)
import           Data.Profunctor.Product.TH
import           Data.Text                  (Text)
import           Control.Lens.TH
import           Servant.Elm
import           Opaleye

import           Model.User
import           Model.Workshop

-- usersPworkshops
data UserWorkshop userId user workshopId workshop description rating =
    UserWorkshop { userWorkshopUserId      :: userId
                 , userWorkshopUser        :: user
                 , userWorkshopWorkshopId  :: workshopId
                 , userWorkshopWorkshop    :: workshop
                 , userWorkshopDescription :: description
                 , userWorkshopRating      :: rating
                 } deriving (Generic, ToJSON, FromJSON, Show, ElmType)

type UserWorkshopWriteColumns = UserWorkshop
    (Column PGInt8)
    UserWriteColumns
    (Column PGInt8)
    WorkshopWriteColumns
    (Column PGText)
    (Column PGInt8)
type UserWorkshopReadColumns = UserWorkshop
    (Column PGInt8)
    UserReadColumns
    (Column PGInt8)
    WorkshopReadColumns
    (Column PGText)
    (Column PGInt8)

type UserWorkshopRead  = UserWorkshop Int64        UserRead   Int64        WorkshopRead   Text Int64
type UserWorkshopWrite = UserWorkshop Int64 (Maybe UserWrite) Int64 (Maybe WorkshopWrite) Text Int64

makeAdaptorAndInstance "pUserWorkshop" ''UserWorkshop
makeLensesWith abbreviatedFields ''UserWorkshop

userWorkshopTable :: Table UserWorkshopWriteColumns UserWorkshopReadColumns
userWorkshopTable = Table "users_workshops" $ pUserWorkshop UserWorkshop
    { userWorkshopUserId = required "user_id"
    , userWorkshopUser = userColumns
    , userWorkshopWorkshopId = required "workshop_id"
    , userWorkshopWorkshop = pWorkshop Workshop
        { workshopId = optional "id"
        , workshopName = required "name"
        }
    , userWorkshopDescription = required "description"
    , userWorkshopRating = required "rating"
    }
