{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE TemplateHaskell        #-}

module Model.User where

import           Control.Arrow
import           Control.Lens
import           Crypto.PasswordStore
import           Data.Aeson
import qualified Data.Binary                as Bin
import qualified Data.ByteString            as BS
import qualified Data.ByteString.Lazy       as BL
import           Data.Int                   (Int64)
import           Data.Profunctor.Product.TH
import           Data.Text                  (Text)
import           Data.Time                  (Day)
import           Database.PostgreSQL.Simple (Connection)
import           GHC.Generics
import           Opaleye
import           Prelude                    hiding (id)
import qualified Prelude                    (id)
import           Safe
import           Servant.Elm

import           Common.Database

-- | Models a user with variable types of fields.
data User id email passHash salt address address2 address3 bd isAdmin name phone postcode =
    User { userId       :: id       -- ^ user id
         , userEmail    :: email    -- ^ email
         , userPassHash :: passHash -- ^ hashed password
         , userSalt     :: salt     -- ^ salt for hashing
         , userName     :: name     -- ^ name
         , userAddress  :: address  -- ^ address (1st line)
         , userAddress2 :: address2 -- ^ address (2nd line)
         , userAddress3 :: address3 -- ^ address (3rd line)
         , userPostcode :: postcode -- ^ postcode
         , userBirthday :: bd       -- ^ birthday
         , userPhone    :: phone    -- ^ phone number
         , userIsAdmin  :: isAdmin  -- ^ whether the user has admin priveleges
         } deriving (Generic, Show, ElmType)

-- Generating product-profunctor Default instances for User.
makeAdaptorAndInstance "pUser" ''User

-- Generating lenses for User using camelCaseFields rules (https://hackage.haskell.org/package/lens-4.15.3/docs/Control-Lens-TH.html)
makeLensesWith camelCaseFields ''User

-- | Type synonym for Text
type Email = Text

-- | Type synonym for Text
type Password = Text

-- | Type synonym for strict ByteString
type PassHash = BS.ByteString

-- | Type synonym for strict ByteStrict
type PassSalt = BS.ByteString

-- | This type is only used when a user signs up.
-- This type doesn't have an id and contains a non-encrypted password.
data NewUser = NewUser
    { newUserEmail    :: Email
    , newUserPassword :: Password
    , newUserName     :: Text
    , newUserAddress  :: Text       -- ^ address (1st line)
    , newUserAddress2 :: Maybe Text -- ^ address (2nd line) (optional)
    , newUserAddress3 :: Maybe Text -- ^ address (3rd line) (optional)
    , newUserPostcode :: Text
    , newUserBirthday :: Day
    , newUserPhone    :: Text       -- ^ phone number
    } deriving (Generic, ToJSON, FromJSON, Show, ElmType)

-- | This type exists so that I don't need to send user's encrypted password and salt.
data PublicUser = PublicUser
    { publicUserId       :: Id
    , publicUserEmail    :: Email
    , publicUserName     :: Text
    , publicUserAddress  :: Text
    , publicUserAddress2 :: Maybe Text
    , publicUserAddress3 :: Maybe Text
    , publicUserBirthday :: Day
    , publicUserIsAdmin  :: Bool
    , publicUserPhone    :: Text
    , publicUserPostcode :: Text
    } deriving (Generic, ToJSON, FromJSON, Show, ElmType)

-- Generating lenses using camelCaseFields rules (https://hackage.haskell.org/package/lens-4.15.3/docs/Control-Lens-TH.html)
makeLensesWith camelCaseFields ''PublicUser

-- | Type for writing users to the PostgreSQL database
-- id is Maybe so that PostgreSQL can set the right id
type UserWriteColumns =
    User (Maybe (Column PGInt8))
         (Column PGText)
         (Column PGBytea)
         (Column PGBytea)
         (Column PGText)
         (Column (Nullable PGText))
         (Column (Nullable PGText))
         (Column PGDate)
         (Column PGBool)
         (Column PGText)
         (Column PGText)
         (Column PGText)

-- | Type for reading users from the PostgreSQL database
type UserReadColumns =
    User (Column PGInt8)
         (Column PGText)
         (Column PGBytea)
         (Column PGBytea)
         (Column PGText)
         (Column (Nullable PGText))
         (Column (Nullable PGText))
         (Column PGDate)
         (Column PGBool)
         (Column PGText)
         (Column PGText)
         (Column PGText)

-- | Type for working with users in Haskell after reading from the database
type UserRead =
    User Id
         Text
         BS.ByteString
         BS.ByteString
         Text
         (Maybe Text)
         (Maybe Text)
         Day
         Bool
         Text
         Text
         Text

-- | Type for working with users in Haskell before writing to the database
type UserWrite =
    User (Maybe Int64)
         Text
         BS.ByteString
         BS.ByteString
         Text
         (Maybe Text)
         (Maybe Text)
         Day
         Bool
         Text
         Text
         Text

-- | Table properties of the "users" table from the database
userColumns :: TableProperties UserWriteColumns UserReadColumns
userColumns =
    pUser User { userId       = optional "id"
               , userEmail    = required "email"
               , userPassHash = required "passhash"
               , userSalt     = required "salt"
               , userAddress  = required "address"
               , userAddress2 = required "address2"
               , userAddress3 = required "address3"
               , userBirthday = required "birthday"
               , userIsAdmin  = required "is_admin"
               , userName     = required "name"
               , userPhone    = required "phone"
               , userPostcode = required "postcode"
               }

-- | Table that connects the "users" table from the database to Haskell
userTable :: Table UserWriteColumns UserReadColumns
userTable = Table "users" userColumns

-- | Takes text and salt, returns the hashed text.
encryptPassword :: Text -> Salt -> BS.ByteString
encryptPassword password s = makePasswordSalt (BL.toStrict $ Bin.encode password) s 17

-- | Takes NewUser, generates random salt, encrypts the user's password and returns UserWrite.
toUserWrite :: NewUser -> IO UserWrite
toUserWrite newUser = makeUser <$> genSaltIO
    where makeUser s =
            User { userId       = Nothing
                 , userEmail    = newUserEmail newUser
                 , userPassHash = encryptPassword (newUserPassword newUser) s
                 , userSalt     = exportSalt s
                 , userAddress  = newUserAddress newUser
                 , userAddress2 = newUserAddress2 newUser
                 , userAddress3 = newUserAddress3 newUser
                 , userBirthday = newUserBirthday newUser
                 , userIsAdmin  = False
                 , userName     = newUserName newUser
                 , userPhone    = newUserPhone newUser
                 , userPostcode = newUserPostcode newUser
                 }

-- | Converts UserRead to PublicUser by removing the password and the salt.
toPublicUser :: UserRead -> PublicUser
toPublicUser user =
    PublicUser { publicUserId         = user ^. id
               , publicUserEmail      = user ^. email
               , publicUserAddress    = user ^. address
               , publicUserAddress2   = user ^. address2
               , publicUserAddress3   = user ^. address3
               , publicUserBirthday   = user ^. birthday
               , publicUserIsAdmin    = user ^. isAdmin
               , publicUserName       = user ^. name
               , publicUserPhone      = user ^. phone
               , publicUserPostcode   = user ^. postcode
               }

-- | Takes a connection to the database and returns all users from the database.
getUsers :: Connection -> IO [UserRead]
getUsers conn = runQuery conn $ queryTable userTable

-- | Takes a connection to the database and id, returns the user with this id if such exists.
getUserWithId :: Connection -> Id -> IO (Maybe UserRead)
getUserWithId conn uid = fmap headMay $ runQuery conn $ queryTable userTable >>> filterSql ((.==) (constant uid) . userId)

-- | Takes a connection to the database and an email, returns the user with this email if such exists.
getUserWithEmail :: Connection -> Email -> IO (Maybe UserRead)
getUserWithEmail conn emailAddress =
    fmap headMay $ runQuery conn $ queryTable userTable >>> filterSql ((.==) (constant emailAddress) . userEmail)

-- | Takes a connection to the database, and user details. Updates the user in the database and returns the updated version.
updateUserWithId :: Connection -> PublicUser -> IO (Maybe UserRead)
updateUserWithId conn changedUser =
    headMay <$> runUpdateReturning conn userTable updateColumns (\user -> user ^. id .== constant (changedUser ^. id)) Prelude.id
    where updateColumns :: UserReadColumns -> UserWriteColumns
          updateColumns user =
              User { userId       = Just $ user ^. id
                   , userEmail    = constant $ changedUser ^. email
                   , userName     = constant $ changedUser ^. name
                   , userAddress  = constant $ changedUser ^. address
                   , userAddress2 = constant $ changedUser ^. address2
                   , userAddress3 = constant $ changedUser ^. address3
                   , userPostcode = constant $ changedUser ^. postcode
                   , userBirthday = constant $ changedUser ^. birthday
                   , userPhone    = constant $ changedUser ^. phone
                   , userIsAdmin  = constant $ changedUser ^. isAdmin
                   , userPassHash = user ^. passHash
                   , userSalt     = user ^. salt
                   }

-- | Takes a connection to the database and a new user.
-- Encrypts the password, creates a new record in the database, and returns the new record.
createUser :: Connection -> NewUser -> IO (Maybe UserRead)
createUser conn newUser =
    toUserWrite newUser >>= \user -> headMay <$> runInsertReturning conn userTable (constant user) Prelude.id


-- | Takes a connection to the database and user id, deletes the user from the database.
deleteUserWithId :: Connection -> Id -> IO Bool
deleteUserWithId conn uid = (== 1) <$> runDelete conn userTable ((.== constant uid) . userId)
