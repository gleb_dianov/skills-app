{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE TemplateHaskell        #-}

{-|
Module      : Model.UserSkill
Description : Module for working with users' skills.
-}

module Model.UserSkill where

import           Control.Arrow
import           Control.Lens
import           Data.Aeson
import           Data.Int                   (Int64)
import           Data.Profunctor.Product.TH
import           Database.PostgreSQL.Simple
import           GHC.Generics
import           Opaleye
import           Prelude                    hiding (id)
import qualified Prelude                    (id)
import           Safe
import           Servant.Elm

import           Common.Database
import           Model.Skill                hiding (skillId)

-- | Type synonym for Int64
type SkillId = Id

-- | Type synonym for Int64
type UserId = Id

-- | Models a user's skill with variable types of fields.
data UserSkill skillId userId percentage = UserSkill
    { userSkillSkillId    :: skillId
    , userSkillUserId     :: userId
    , userSkillPercentage :: percentage
    } deriving (Generic, ToJSON, FromJSON, Show, ElmType)

-- | Type for writing users' skills to the PostgreSQL database.
type UserSkillWriteColumns = UserSkill (Column PGInt8) (Column PGInt8) (Column PGInt8)

-- | Type for reading users' skills from the PostgreSQL database.
type UserSkillReadColumns = UserSkillWriteColumns

-- | Type for working with users' skills in Haskell after reading from the database.
type UserSkillRead = UserSkill SkillId UserId Int64

-- | Type for working with users' skills in Haskell before writing to the database.
type UserSkillWrite = UserSkillRead

-- Generating product-profunctor Default instances for UserSkill.
makeAdaptorAndInstance "pUserSkill" ''UserSkill

-- Generating lenses for UserSkill using camelCaseFields rules (https://hackage.haskell.org/package/lens-4.15.3/docs/Control-Lens-TH.html)
makeLensesWith camelCaseFields ''UserSkill

-- | Table that connects the "users_skills" table from the database to Haskell
userSkillTable :: Table UserSkillWriteColumns UserSkillReadColumns
userSkillTable = Table "users_skills" $ pUserSkill UserSkill
    { userSkillSkillId = required "skill_id"
    , userSkillUserId = required "user_id"
    , userSkillPercentage = required "percentage"
    }

-- | Type for adding percentage to a skill.
data SkillWithPercentage percentage skill =
    SkillWithPercentage { skillPercentage :: percentage
                        , usersSkill      :: skill
                        } deriving (Generic, ElmType, ToJSON)

-- Generating product-profuncto Default instance for SkillWithPercentage.
makeAdaptorAndInstance "pSkillWithPercentage" ''SkillWithPercentage

-- | Type for reading skills with percentages from the PostgreSQL database.
type SkillWithPercentageReadColumns = SkillWithPercentage (Column PGInt8) SkillReadColumns

-- | Type for working with skills with percentages after reading them from the database.
type SkillWithPercentageRead = SkillWithPercentage Int64 SkillRead

-- | Takes a connection to the database and user id and returns user's skills with percentages.
getUserSkillsWithUserId :: Connection -> Id -> IO [SkillWithPercentageRead]
getUserSkillsWithUserId conn uid = runQuery conn $ querySkillsWithPercentageForUserId $ constant uid

-- | Takes a connection to the database, uid, skill id, and percentage.
-- Associates skill with the user and returns the skill with percentage.
associateSkill :: Connection -> UserId -> SkillId -> Int64 -> IO (Maybe SkillWithPercentageRead)
associateSkill conn uid sid perc = headMay <$> (runInsertManyReturning conn userSkillTable [constant $ UserSkill sid uid perc] Prelude.id :: IO [UserSkillRead])
                               >>= maybe (return Nothing) (\swp -> maybe Nothing (\s -> Just $ SkillWithPercentage (swp ^. percentage) s) <$> getSkillWithId conn (swp ^. skillId))

-- | Takes a user id and returns a query that finds that user's skills.
querySkillsWithPercentageForUserId :: Column PGInt8 -> QueryArr () SkillWithPercentageReadColumns
querySkillsWithPercentageForUserId uid = (queryTable userSkillTable &&& queryTable skillTable)
                                     >>> filterSql (\(userSkill, _) -> userSkill ^. userId .== uid)
                                     >>> filterSql (\(userSkill, skill) -> userSkill ^. skillId .== skill ^. Model.Skill.id)
                                     >>> arr (\(userSkill, skill) -> SkillWithPercentage (userSkill ^. percentage) skill)

-- | Takes a connection to the database, user id, skill id, deletes skill association. Returns True wrapped in IO monad if one column was deleted.
deleteUserSkill :: Connection -> UserId -> SkillId -> IO Bool
deleteUserSkill conn uid sid = (== 1) <$> runDelete conn userSkillTable (\us -> us ^. userId .== constant uid .&& us ^. skillId .== constant sid)

-- | Represents a new skill and a new user's skill.
data NewUserSkill =
    NewUserSkill { newUserSkillSkill      :: SkillWrite
                 , newUserSkillPercentage :: Int64
                 } deriving (Generic, FromJSON, ElmType)


-- | Takes a connection to the database, a user skill and adds it to the database. Returns the new skill with percentage if succeeds.
createUserSkill :: Connection -> UserId -> NewUserSkill -> IO (Maybe SkillWithPercentageRead)
createUserSkill conn uid newUS = createSkill conn (newUserSkillSkill newUS)
                             >>= maybe (return Nothing) (\skill -> associateSkill conn uid (skill ^. id) (newUserSkillPercentage newUS))
