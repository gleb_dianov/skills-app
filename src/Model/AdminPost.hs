{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE TemplateHaskell        #-}

{-|
Module      : Model.AdminPost
Description : Module for working with admin posts.
-}

module Model.AdminPost where

import           Control.Lens
import           Control.Lens.TH
import           Data.Aeson
import           Data.Int                   (Int64)
import           Data.Profunctor.Product.TH
import           Data.Text                  (Text)
import           Database.PostgreSQL.Simple
import           GHC.Generics
import           Opaleye
import           Safe
import           Servant.Elm                (ElmType)

import           Common.Database

-- | Models an admin post with variable types of fields.
data AdminPost id title message = AdminPost
    { adminPostId      :: id
    , adminPostTitle   :: title
    , adminPostMessage :: message
    } deriving (Generic, ToJSON, FromJSON, Show, ElmType)

-- | This type is just for creating new admin posts.
data NewAdminPost = NewAdminPost
    { newAdminPostTitle   :: Text
    , newAdminPostMessage :: Text
    } deriving (Generic, ToJSON, FromJSON, Show, ElmType)

-- | Type for writing admin posts to the PostgreSQL database.
type AdminPostWriteColumns = AdminPost (Maybe (Column PGInt8)) (Column PGText) (Column PGText)

-- | Type for reading admin posts from the PostgreSQL database.
type AdminPostReadColumns = AdminPost (Column PGInt8) (Column PGText) (Column PGText)

-- | Type for working with admin posts after reading them from the database.
type AdminPostRead = AdminPost Id Text Text

-- | Type for working with admin posts before writing the to the database.
type AdminPostWrite = AdminPost (Maybe Id) Text Text

-- Generating product-profunctor Default instances for Skill
makeAdaptorAndInstance "pAdminPost" ''AdminPost

-- Generating lenses using camelCaseFields rules (https://hackage.haskell.org/package/lens-4.15.3/docs/Control-Lens-TH.html)
makeLensesWith camelCaseFields ''AdminPost

-- | Table that connects the "admin_posts" table from the database to Haskell
adminPostTable :: Table AdminPostWriteColumns AdminPostReadColumns
adminPostTable = Table "admin_posts" $ pAdminPost
    AdminPost { adminPostId = optional "id"
              , adminPostTitle = required "title"
              , adminPostMessage = required "message"
              }

-- | Converts NewAdminPost to AdminPostWrite
toAdminPostWrite :: NewAdminPost -> AdminPostWrite
toAdminPostWrite newAdminPost =
    AdminPost { adminPostId      = Nothing
              , adminPostTitle   = newAdminPostTitle newAdminPost
              , adminPostMessage = newAdminPostMessage newAdminPost
              }

-- | Takes a connection to the database and returns all admin posts.
getAdminPosts :: Connection -> IO [AdminPostRead]
getAdminPosts conn = runQuery conn $ queryTable adminPostTable

-- | Takes a connection to the server and a new admin post, adds the post to the database, and returns the new record.
createAdminPost :: Connection -> NewAdminPost -> IO (Maybe AdminPostRead)
createAdminPost conn newPost = headMay <$> runInsertReturning conn adminPostTable (constant $ toAdminPostWrite newPost) Prelude.id
