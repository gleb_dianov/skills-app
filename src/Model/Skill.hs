{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE TemplateHaskell        #-}

{-|
Module      : Model.Skill
Description : Module for working with skills.
-}

module Model.Skill where

import           Control.Arrow
import           Control.Lens.TH
import           Data.Aeson
import           Data.Profunctor.Product.TH
import           Data.Text                  (Text)
import           Database.PostgreSQL.Simple (Connection)
import           GHC.Generics
import           Opaleye
import           Safe
import           Servant.Elm

import           Common.Database

-- | Models a skill with variable types of fields.
data Skill id name = Skill
    { skillId   :: id
    , skillName :: name
    } deriving (Generic, ToJSON, FromJSON, Show, ElmType)

-- | Type for writing skills to the PostgreSQL database.
-- id is Maybe so that PostgreSQL can set the right id
type SkillWriteColumns = Skill (Maybe (Column PGInt8)) (Column PGText)

-- | Type for reading skills from the PostgreSQL database.
type SkillReadColumns = Skill        (Column PGInt8)  (Column PGText)

-- | Type for working with skills after reading from the database.
type SkillRead = Skill Id Text

-- | Type for working with skills before writing to the database.
type SkillWrite = Skill (Maybe Id) Text

-- Generating product-profunctor Default instances for Skill
makeAdaptorAndInstance "pSkill" ''Skill

-- Generating lenses using camelCaseFields rules (https://hackage.haskell.org/package/lens-4.15.3/docs/Control-Lens-TH.html)
makeLensesWith camelCaseFields ''Skill

-- | Table that connects the "skills" table from the database to Haskell
skillTable :: Table SkillWriteColumns SkillReadColumns
skillTable = Table "skills" $ pSkill Skill
    { skillId = optional "id"
    , skillName = required "name"
    }

-- | Takes a connection to the database and returns all skills.
getSkills :: Connection -> IO [SkillRead]
getSkills conn = runQuery conn $ queryTable skillTable

-- | Takes a connection to the database and skill id, returns the skill with this skill id if it exists.
getSkillWithId :: Connection -> Id -> IO (Maybe SkillRead)
getSkillWithId conn sid = fmap headMay $ runQuery conn $ queryTable skillTable >>> filterSql ((.==) (constant sid) . skillId)

-- | Takes a connection to the database and a new skill, creates record for it in the database.
-- Returns the new skill if succeeds.
createSkill :: Connection -> SkillWrite -> IO (Maybe SkillRead)
createSkill conn newSkill = headMay <$> runInsertManyReturning conn skillTable [constant newSkill] Prelude.id
