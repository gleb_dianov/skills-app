all: server client

server:
		stack build

client: Api.elm
		elm make client/src/Main.elm --output assets/index.html --yes

Api.elm:
		stack runhaskell app/GenerateElm.hs

clean: clean-client clean-server

clean-client:
		rm -v client/src/Generated/Api.elm
		rm -v ./assets/index.html

clean-server:
		stack clean
