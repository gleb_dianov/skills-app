CREATE TABLE "admin_posts" (
	"id" serial NOT NULL,
	"title" TEXT NOT NULL,
	"message" TEXT NOT NULL UNIQUE,
	CONSTRAINT admin_posts_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "skills" (
	"id" serial NOT NULL,
	"name" TEXT NOT NULL UNIQUE,
	CONSTRAINT skills_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "users_skills" (
	"skill_id" integer NOT NULL,
	"user_id" serial NOT NULL,
	"percentage" integer NOT NULL,
	CONSTRAINT users_skills_pk PRIMARY KEY ("skill_id","user_id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "users" (
	"id" serial NOT NULL,
	"passhash" BYTEA NOT NULL,
	"address" TEXT NOT NULL,
	"salt" BYTEA NOT NULL,
	"email" TEXT NOT NULL UNIQUE,
	"address2" TEXT,
	"address3" TEXT,
	"birthday" DATE NOT NULL,
	"is_admin" BOOLEAN NOT NULL,
	"name" TEXT NOT NULL UNIQUE,
	"phone" TEXT NOT NULL UNIQUE,
	"postcode" TEXT NOT NULL,
	CONSTRAINT users_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "users_workshops" (
	"user_id" integer NOT NULL,
	"workshop_id" integer NOT NULL,
	"description" TEXT NOT NULL,
	"rating" integer NOT NULL,
	CONSTRAINT users_workshops_pk PRIMARY KEY ("user_id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "workshops" (
	"id" serial NOT NULL,
	"name" TEXT NOT NULL UNIQUE,
	CONSTRAINT workshops_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



ALTER TABLE "users_skills" ADD CONSTRAINT "users_skills_fk0" FOREIGN KEY ("skill_id") REFERENCES "skills"("id") ON DELETE CASCADE;
ALTER TABLE "users_skills" ADD CONSTRAINT "users_skills_fk1" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE;


ALTER TABLE "users_workshops" ADD CONSTRAINT "users_workshops_fk0" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE;
ALTER TABLE "users_workshops" ADD CONSTRAINT "users_workshops_fk1" FOREIGN KEY ("workshop_id") REFERENCES "workshops"("id") ON DELETE CASCADE;


